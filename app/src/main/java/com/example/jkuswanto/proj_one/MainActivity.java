package com.example.jkuswanto.proj_one;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity {

    private Spinner spinner;
    private EditText input_age;
    private TextView bear_yrs;
    private TextView cat_yrs;
    private TextView dog_yrs;
    private TextView hamster_yrs;
    private TextView hippo_yrs;
    private TextView human_yrs;
    private TextView kangaroo_yrs;
    public static final String[] ANIMALS = {"Bear", "Cat", "Dog", "Hamster", "Hippopotamus", "Human", "Kangaroo"};
    public static final Map<String, Double> ANIMAL_CONVERSION = new HashMap<String, Double>() {
        {
            put("Bear", 2.0);
            put("Cat", 3.2);
            put("Dog", 3.64);
            put("Hamster", 20.0);
            put("Hippopotamus", 1.78);
            put("Human", 1.0);
            put("Kangaroo", 8.89);
        }
    };
    private String animal;
    private double age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addSpinner();
        animal = spinner.getSelectedItem().toString();
        input_age = (EditText) findViewById(R.id.get_age);
        bear_yrs = (TextView) findViewById(R.id.bear_age);
        cat_yrs =(TextView) findViewById(R.id.cat_age);
        dog_yrs =(TextView) findViewById(R.id.dog_age);
        hamster_yrs =(TextView) findViewById(R.id.ham_age);
        hippo_yrs =(TextView) findViewById(R.id.hip_age);
        human_yrs =(TextView) findViewById(R.id.hum_age);
        kangaroo_yrs =(TextView) findViewById(R.id.kanga_age);

        input_age.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                age = 0.0;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    age = Double.parseDouble(input_age.getText().toString());
                    String Tag = "MyClass";
                    Log.d(Tag, "age" + age);
                    Log.d(Tag, animal);

                    //Convert the ages of the animals correctly
                    Map<String, Double> animal_ages = get_other_ages();
                    change_ages(animal_ages);

                } catch (NumberFormatException e) {
                }
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void addSpinner() {
        spinner = (Spinner) findViewById(R.id.animal_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.animals_arr, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                animal = parent.getItemAtPosition(position).toString();
                Map<String, Double> animal_ages = get_other_ages();
                change_ages(animal_ages);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner.setAdapter(adapter);
    }

    private double convert_to_human_yrs(){
        return age / ANIMAL_CONVERSION.get(animal);
    }

    private Map get_other_ages(){
        double human_yrs = convert_to_human_yrs();
        Map<String, Double> other_ages = new HashMap<String, Double>();

        for (int i = 0; i < ANIMALS.length; i++) {
            other_ages.put(ANIMALS[i], human_yrs * ANIMAL_CONVERSION.get(ANIMALS[i]));
        }
        return other_ages;
    }

    private void change_ages(Map animal_ages){
        bear_yrs.setText(show_to_tenth_place(animal_ages.get("Bear").toString()) + " ");
        cat_yrs.setText(show_to_tenth_place(animal_ages.get("Cat").toString()) + " ");
        dog_yrs.setText(show_to_tenth_place(animal_ages.get("Dog").toString()) + " ");
        hamster_yrs.setText(show_to_tenth_place(animal_ages.get("Hamster").toString()) + " ");
        hippo_yrs.setText(show_to_tenth_place(animal_ages.get("Hippopotamus").toString()) + " ");
        human_yrs.setText(show_to_tenth_place(animal_ages.get("Human").toString()) + " ");
        kangaroo_yrs.setText(show_to_tenth_place(animal_ages.get("Kangaroo").toString()) + " ");
    }

    private String show_to_tenth_place(String num){
        for (int i = 0; i < num.length(); i++) {
            if (num.charAt(i) == '.') {
                return num.substring(0,i+2);
            }
        }
        return "0.0";
    }
}